<?php

namespace ClientExport\Strategy;

use ClientExport\Entity\Client;

interface ExportStrategyInterface
{
    /**
     * @param array|Client[] $clients
     */
    public function export(array $clients): void;
}