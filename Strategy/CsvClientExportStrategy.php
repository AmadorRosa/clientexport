<?php

namespace ClientExport\Strategy;

use ClientExport\Entity\Client;

class CsvClientExportStrategy implements ExportStrategyInterface
{
    /**
     * @param Client[] $clients
     */
    public function export(array $clients): void
    {
        if(count($clients ) > 0) {
            $fp = fopen(
                __DIR__ .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'data' .
                DIRECTORY_SEPARATOR .
                microtime() . '.csv',
                'w'
            );
            foreach ($clients as $client) {
                fputcsv($fp, $client->toArray());
            }
            fclose($fp);
        }
    }
}