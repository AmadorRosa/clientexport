<?php

namespace ClientExport;

use ClientExport\DataSources\ClientDataSourceInterface;
use ClientExport\Entity\Client;
use ClientExport\Strategy\ExportStrategyInterface;

class ClientsExportService
{
    /**
     * @var ClientDataSourceInterface[]
     */
    private $dataSources;

    /**
     * @var ExportStrategyInterface
     */
    private $exportStrategy;

    /**
     * @var Client[]
     */
    private $clients = [];

    /**
     * ClientsExportService constructor.
     * @param ClientDataSourceInterface[] $dataSources
     * @param ExportStrategyInterface $exportStrategy
     */
    public function __construct(array $dataSources, ExportStrategyInterface $exportStrategy)
    {
        $this->dataSources = $dataSources;
        $this->exportStrategy = $exportStrategy;
    }

    public function export(): void
    {
        foreach ($this->dataSources as $dataSource)
        {
            $this->clients = array_merge($this->clients, $dataSource->extract());
        }
        $this->exportStrategy->export($this->clients);
    }
}