<?php

namespace ClientExport\DataSources;

use ClientExport\Entity\Client;

interface ClientDataSourceInterface
{
    /**
     * @return array|Client
     */
    public function extract(): array;
}