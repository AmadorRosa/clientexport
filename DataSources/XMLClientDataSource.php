<?php

namespace ClientExport\DataSources;

use ClientExport\Entity\Client;

class XMLClientDataSource implements  ClientDataSourceInterface
{
    const SOURCE_PATH = 'file_path.xml'; //TODO Change this with the real filepath

    /**
     * @var Client[]
     */
    private $clients = [];

    public function extract(): array
    {
        $xml = simplexml_load_file(self::SOURCE_PATH);
        $clientLines = $xml->xpath('suitable/xpath');//TODO Change this with the suitable xpath
        foreach ($clientLines as $clientLine)
        {
            //TODO It is very alike you will need to change this construction to match the xml structure
            $this->clients[] = new Client(
                $clientLine->name,
                $clientLine->email,
                $clientLine->direction
            );
        }

        return $this->clients;
    }
}