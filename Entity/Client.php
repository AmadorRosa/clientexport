<?php

namespace ClientExport\Entity;

class Client
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $direccion;

    /**
     * Client constructor.
     * @param string $name
     * @param string $email
     * @param string $direccion
     */
    public function __construct(string $name, string $email, string $direccion)
    {
        $this->name = $name;
        $this->email = $email;
        $this->direccion = $direccion;
    }

    public function toArray()
    {
        return [
            $this->name,
            $this->email,
            $this->direccion
        ];
    }
}